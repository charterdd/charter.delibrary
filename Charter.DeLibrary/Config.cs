﻿using Rocket.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Charter.DeLibrary
{
    public class Config : IRocketPluginConfiguration
    {
        public string DatabaseAddress;
        public string DatabaseName;
        public string DatabaseUsername;
        public string DatabasePassword;
        public int DatabasePort;
        public string TableDeads;
        public string TableIPs;
        public string Localization;
        public void LoadDefaults()
        {
            DatabaseAddress = "localhost";
            DatabaseName = "unturned";
            DatabaseUsername = "owner";
            DatabasePassword = "hardpass";
            DatabasePort = 3306;
            TableDeads = "C.PlayerDeads";
            TableIPs = "C.PlayerIPs";
            Localization = "English";
        }
    }
}
