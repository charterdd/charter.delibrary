﻿using fr34kyn01535.Uconomy;
using Rocket.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Charter.DeLibrary
{
    public class LoadMySQL : IRocketCommand
    {
        public AllowedCaller AllowedCaller => AllowedCaller.Both;

        public string Name => "loadmysql";

        public string Help => "";

        public string Syntax => "";

        public List<string> Aliases => new List<string>();

        public List<string> Permissions => new List<string>();

        public void Execute(IRocketPlayer caller, string[] command)
        {
            if (command.Length != 1)
            {
                return;
            }
            switch (command[0])
            {
                case "uconomy":
                    Plug.Instance.Configuration.Instance.DatabaseAddress = Uconomy.Instance.Configuration.Instance.DatabaseAddress;
                    Plug.Instance.Configuration.Instance.DatabaseName = Uconomy.Instance.Configuration.Instance.DatabaseName;
                    Plug.Instance.Configuration.Instance.DatabaseUsername = Uconomy.Instance.Configuration.Instance.DatabaseUsername;
                    Plug.Instance.Configuration.Instance.DatabasePassword = Uconomy.Instance.Configuration.Instance.DatabasePassword;
                    Plug.Instance.Configuration.Save();
                    break;
            }
        }
    }
}
