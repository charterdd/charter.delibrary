﻿using Rocket.Core.Plugins;
using Rocket.Unturned;
using Rocket.Unturned.Events;
using Rocket.Unturned.Player;
using SDG.Unturned;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Charter.DeLibrary
{
    public class Plug : RocketPlugin<Config>
    {
        public static Plug Instance;
        DatabaseManager Database;
        protected override void Load()
        {
            Instance = this;
            Database = new DatabaseManager();
            UnturnedPlayerEvents.OnPlayerDeath += Death;
            U.Events.OnPlayerConnected += Conn;
        }

        private void Conn(UnturnedPlayer player)
        {
            Database.SetUp(player);
        }

        private void Death(UnturnedPlayer player, EDeathCause cause, ELimb limb, CSteamID murderer)
        {
            switch (cause)
            {
                case EDeathCause.ACID:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Acid");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Яд");
                    break;
                case EDeathCause.ANIMAL:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Animal");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Животное");
                    break;
                case EDeathCause.ARENA:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Arena");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Арена");
                    break;
                case EDeathCause.BLEEDING:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Bleeding");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Кровотечение");
                    break;
                case EDeathCause.BONES:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Bones");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Перелом");
                    break;
                case EDeathCause.BOULDER:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Big zombie's boulder");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Камень большого зомби");
                    break;
                case EDeathCause.BREATH:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Breath");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Задохнулся");
                    break;
                case EDeathCause.BURNER:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Burned");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Огонь");
                    break;
                case EDeathCause.BURNING:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Burning");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Огонь");
                    break;
                case EDeathCause.CHARGE:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Unknown");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Неизвестно");
                    break;
                case EDeathCause.FOOD:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "FOOD");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Голод");
                    break;
                case EDeathCause.FREEZING:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Freezing");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Холод");
                    break;
                case EDeathCause.GRENADE:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Grenade");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Граната");
                    break;
                case EDeathCause.GUN:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "GUN");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Убит");
                    break;
                case EDeathCause.INFECTION:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Infection");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Заражение");
                    break;
                case EDeathCause.KILL:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Kill");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Убит");
                    break;
                case EDeathCause.LANDMINE:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Landmine");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Мина");
                    break;
                case EDeathCause.MELEE:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Melee");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Холодное оружие");
                    break;
                case EDeathCause.MISSILE:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Missile");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Взрывной патрон");
                    break;
                case EDeathCause.PUNCH:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Punch");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Кулаками");
                    break;
                case EDeathCause.ROADKILL:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Roadkill");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Убийство машиной");
                    break;
                case EDeathCause.SENTRY:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Sentry");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Турель");
                    break;
                case EDeathCause.SHRED:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Unknown");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Неизвестно");
                    break;
                case EDeathCause.SPARK:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Unknown");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Неизвестно");
                    break;
                case EDeathCause.SPIT:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Unknown");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Неизвестно");
                    break;
                case EDeathCause.SPLASH:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Splash");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, UnturnedPlayer.FromCSteamID(murderer), "Сплэш");
                    break;
                case EDeathCause.SUICIDE:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Suicide");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Суицид");
                    break;
                case EDeathCause.VEHICLE:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Vehicle");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Машина");
                    break;
                case EDeathCause.WATER:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Water");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Жажда");
                    break;
                case EDeathCause.ZOMBIE:
                    if (Configuration.Instance.Localization == "English")
                        Database.AddKill(player, "Zombie");
                    if (Configuration.Instance.Localization == "Russian")
                        Database.AddKill(player, "Зомби");
                    break;
            }
        }

        protected override void Unload()
        {
            UnturnedPlayerEvents.OnPlayerDeath -= Death;
            U.Events.OnPlayerConnected -= Conn;
        }
    }
}
