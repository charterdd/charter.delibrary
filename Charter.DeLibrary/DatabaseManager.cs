﻿using MySql.Data.MySqlClient;
using Rocket.Core.Logging;
using Rocket.Unturned.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Charter.DeLibrary
{
    public class DatabaseManager
    {
        internal DatabaseManager()
        {
            CheckSchema();
        }

        private MySqlConnection createConnection()
        {
            MySqlConnection result = null;
            try
            {
                result = new MySqlConnection(string.Format("SERVER={0};DATABASE={1};UID={2};PASSWORD={3};PORT={4};", new object[]
                {
                    Plug.Instance.Configuration.Instance.DatabaseAddress,
                    Plug.Instance.Configuration.Instance.DatabaseName,
                    Plug.Instance.Configuration.Instance.DatabaseUsername,
                    Plug.Instance.Configuration.Instance.DatabasePassword,
                    Plug.Instance.Configuration.Instance.DatabasePort
                }));
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, null);
            }
            return result;
        }

        internal void CheckSchema()
        {
            try
            {
                //kills
                MySqlConnection mySqlConnectionone = createConnection();
                MySqlCommand mySqlCommand = mySqlConnectionone.CreateCommand();
                mySqlCommand.CommandText = "show tables like '" + Plug.Instance.Configuration.Instance.TableDeads + "'";
                mySqlConnectionone.Open();
                if (mySqlCommand.ExecuteScalar() == null)
                {
                    mySqlCommand.CommandText = "CREATE TABLE `" + Plug.Instance.Configuration.Instance.TableDeads + "` (`DeadSTEAMID` varchar(32) NOT NULL,`DeadName` varchar(64) NOT NULL,`DeadReason` varchar(32) NOT NULL, `KillerSTEAM64ID` varchar(32), `KillerName` varchar(64)) ";
                    mySqlCommand.ExecuteNonQuery();
                }
                mySqlConnectionone.Close();

                //ips
                mySqlConnectionone = createConnection();
                mySqlCommand = mySqlConnectionone.CreateCommand();
                mySqlCommand.CommandText = "show tables like '" + Plug.Instance.Configuration.Instance.TableIPs + "'";
                mySqlConnectionone.Open();
                if (mySqlCommand.ExecuteScalar() == null)
                {
                    mySqlCommand.CommandText = "CREATE TABLE `" + Plug.Instance.Configuration.Instance.TableIPs + "` (`PlayerID` varchar(20) NOT NULL,`PlayerName` varchar(64) NOT NULL,`IP` varchar(32) NOT NULL) ";
                    mySqlCommand.ExecuteNonQuery();
                }
                mySqlConnectionone.Close();

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, null);
            }
        }

        internal void SetUp(UnturnedPlayer p)
        {
            try
            {
                MySqlConnection mySqlConnection = createConnection();
                MySqlCommand mySqlCommand = mySqlConnection.CreateCommand();
                int exists = 0;
                mySqlCommand.CommandText = "SELECT * FROM `" + Plug.Instance.Configuration.Instance.TableIPs  + "`WHERE PlayerID = '" + p.CSteamID +"'";
                mySqlConnection.Open();
                object result = mySqlCommand.ExecuteScalar();
                if (result != null) Int32.TryParse(result.ToString(), out exists);
                mySqlConnection.Close();

                if (exists == 0)
                {
                    mySqlCommand.CommandText = "insert ignore into `" + Plug.Instance.Configuration.Instance.TableIPs + "` (PlayerID,PlayerName,IP) values('" + p.CSteamID + "','" + p.CharacterName + "','" + p.IP + "')";
                    mySqlConnection.Open();
                    mySqlCommand.ExecuteNonQuery();
                    mySqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, null);
            }
        }

        internal void AddKill(UnturnedPlayer dead, UnturnedPlayer killer, string reason)
        {
            try
            {
                MySqlConnection mySqlConnection = createConnection();
                MySqlCommand mySqlCommand = mySqlConnection.CreateCommand();
                mySqlCommand.CommandText = string.Concat(new object[]
                    {
                        "insert ignore into `",
                        Plug.Instance.Configuration.Instance.TableDeads,
                        "` (DeadSTEAMID,DeadName,DeadReason,KillerSTEAM64ID,KillerName) values(",
                        "'",
                        dead.CSteamID,
                        "','",
                        dead.CharacterName,
                        "','",
                        reason,
                        "','",
                        killer.CSteamID,
                        "','",
                        killer.CharacterName,
                        "')"
                    });
                mySqlConnection.Open();
                mySqlCommand.ExecuteNonQuery();
                mySqlConnection.Close();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, null);
            }
        }

        internal void AddKill(UnturnedPlayer dead, string reason)
        {
            try
            {
                MySqlConnection mySqlConnection = createConnection();
                MySqlCommand mySqlCommand = mySqlConnection.CreateCommand();
                mySqlCommand.CommandText = string.Concat(new object[]
                    {
                        "insert ignore into `",
                        Plug.Instance.Configuration.Instance.TableDeads,
                        "` (DeadSTEAMID,DeadName,DeadReason,KillerSTEAM64ID,KillerName) values(",
                        "'",
                        dead.CSteamID,
                        "','",
                        dead.CharacterName,
                        "','",
                        reason,
                        "','",
                        null,
                        "','",
                        null,
                        "')"
                    });
                mySqlConnection.Open();
                mySqlCommand.ExecuteNonQuery();
                mySqlConnection.Close();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, null);
            }
        }
    }
}
